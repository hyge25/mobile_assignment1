# Assignment 1 - Group Expenses

Please refer to the [Assignments/Assignment 1](https://git.gvk.idi.ntnu.no/course/imt3673/imt3673-2020/-/wikis/Assignments/Assignment-1) Wiki for details.

# Issues, errors and questions

Please use the normal course/lecture project Issue
tracker for logging issues related to assignments. Use "assignments" label.




